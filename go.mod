module gitlab.com/delta10/kubernetes-performance

go 1.15

require (
	github.com/imdario/mergo v0.3.11 // indirect
	github.com/jawher/mow.cli v1.2.0
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e // indirect
	k8s.io/api v0.18.14
	k8s.io/apimachinery v0.18.14
	k8s.io/client-go v0.18.14
)
